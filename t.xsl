<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

  <xsl:output method="html" encoding="UTF-8" indent="yes" />

  <xsl:template match="eac-cpf">
<html>
  <head>
    <title><xsl:call-template name='title' /></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  
    <link rel="stylesheet" href="/static/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/static/css/bootstrap-responsive.min.css" />
    <!-- Le fav and touch icons -->
    <!-- Commented out - update these with your own files -->
    <!--
      <link rel="shortcut icon" href="ico/favicon.ico" />
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png" />
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png" />
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png" />
      <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png" />
    -->

    <!--[if lt IE 9]>
        <script src="/static/js/html5shiv.js"></script>
    <![endif]-->

  </head>
  <body>
    <div class="container-fluid">
      <span id="top"></span>
      <div class="row-fluid"></div>
    </div>
    <script type="text/javascript" src="/static/js/jquery-1.9.1.min.js">//</script>
    <script type="text/javascript" src="/static/js/bootstrap.min.js">//</script>
  </body>
</html>
  </xsl:template>
</xsl:stylesheet>