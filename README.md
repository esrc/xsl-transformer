# Writing an XSL transform can be a pain 

... without good tools. So, if you can afford it, the winner seems
to be OxygenXML.

If you can't afford it then there's this.

- **start_server** will start a basic python HTTP server in the current directory
- **transform** does the heavy lifting.
- **t.xsl** and **static** are a base xsl stylesheet - ready to be copied and customised.

Say you have an XML file - **index.xml** - and you need to develop a transform for it - **t.xsl**.

To transform the XML file:

    :::sh
    transform -d index.xml -t t.xsl > index.html

Now open up a browser and navigate to ${your ip}:2000

If you want to do auto-reload on change (I'm assumming you're working on a remote host over
SSH or something), then the easiest way is probably to add an auto refresh to the file your
producing. 

That or get good with <ALT><TAB> to select your browser then <CTRL>R to reload it. Admittedly, 
this works quite well for me but each to his own...

Back to topic: if you're working on your local system, and you're viewing index.html in a local browser
via a file URL, perhaps take a look at the [LiveReload](http://livereload.com/) browser extension.
Looks interesting